﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace CS441_Project
{
    public partial class ListView : ContentPage
    {
        public IList<ViewModels.Monkey> Monkeys { get; private set; }

        public ListView()
        {
            InitializeComponent();

            Monkeys = new List<ViewModels.Monkey>();
            Monkeys.Add(new ViewModels.Monkey
            {
                Name = "Task1: Library Work",
                Location = "Work Description need to be done",
                ImageUrl = "https://www.csusm.edu/communications/images/branding-images/spirit-logo01.jpg"
            });

            Monkeys.Add(new ViewModels.Monkey
            {
                Name = "Task2: Library Computer Broke",
                Location = "Work Description need to be done",
                ImageUrl = "https://www.csusm.edu/communications/images/branding-images/spirit-logo01.jpg"
            });

            Monkeys.Add(new ViewModels.Monkey
            {
                Name = "Task3: Room 206 Computer out",
                Location = "Work Description need to be done",
                ImageUrl = "https://www.csusm.edu/communications/images/branding-images/spirit-logo01.jpg"
            });

            Monkeys.Add(new ViewModels.Monkey
            {
                Name = "Task4: Room 5503 wall crack",
                Location = "Work Description need to be done",
                ImageUrl = "https://www.csusm.edu/communications/images/branding-images/spirit-logo01.jpg"
            });

            Monkeys.Add(new ViewModels.Monkey
            {
                Name = "Task5: Parking Lot F needs cleaning",
                Location = "Work Description need to be done",
                ImageUrl = "https://www.csusm.edu/communications/images/branding-images/spirit-logo01.jpg"
            });


            BindingContext = this;
        }
    }
}