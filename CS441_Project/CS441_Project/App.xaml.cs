﻿using Xamarin.Forms;
namespace CS441_Project
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new CS441_Project.LoginPage();
            //MainPage = new CS441_Project.ListView();
        }

        protected override void OnStart()
        {
            // Handle when your app starts  
        }
        protected override void OnSleep()
        {
            // Handle when your app sleeps  
        }
        protected override void OnResume()
        {
            // Handle when your app resumes  
        }
    }
}